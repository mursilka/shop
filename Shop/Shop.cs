﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop1
{
    class Shop : IProduced
    {
        List<Product> products = new List<Product>();
        private string _name_Shop;
        private string _address;
        public Shop()
        {
            _name_Shop = "Лучшие цены";
            _address = "Проспект пушкина, дом Колотушкина";
        }

        public void AddFood()
        {
            Console.WriteLine("Введите название товара");
            string name = Console.ReadLine();

            Console.WriteLine("Введите цену товара");
            double price = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите страну производитель");
            string country = Console.ReadLine();


            Product product = new Food(name, price, country);
            products.Add(product);
        }

        public void AddNotFood()
        {
            Console.WriteLine("Введите название товара");
            string name = Console.ReadLine();

            Console.WriteLine("Введите цену товара");
            double price = Convert.ToDouble(Console.ReadLine());


            string country = Country();

            Product product = new Not_food(name, price, country);
            products.Add(product);
        }

        public void Show()
        {

            foreach (Product product in products)
            {
                Console.WriteLine(product);
            }
        }

        public void Remove()
        {
            string _name = Console.ReadLine();

            for (int i = 0; i < products.Count; i++)
            {
                if (_name == products[i].Name)
                {
                    products.Remove(products[i]);
                }
            }

        }

        public void Sort_Price()
        {
            var sortedPoduct = products.OrderBy(p => p.Price);

            foreach (var p in sortedPoduct)
                Console.WriteLine($"{p.Name} - {p.Price}");


        }

        public void Sort_Country()
        {
            var sortedPoduct = products.OrderBy(p => p.Country);

            foreach (var p in sortedPoduct)
                Console.WriteLine($"{p.Name} - {p.Price} - {p.Country}");


        }

        public void Show_Clas()
        {
            int _choice = Convert.ToInt32(Console.ReadLine());
            if (_choice == 1)
            {
                foreach (Product product in products)
                {
                    if (product.Id == 1)
                        Console.WriteLine(product);
                }
            }
            else
            {
                foreach (Product product in products)
                {
                    if (product.Id == 2)
                        Console.WriteLine(product);
                }
            }
        }

        public string Country()
        {
            Console.WriteLine("Введите страну производитель");
            string _country = Console.ReadLine();

            return _country;
        }
    }
}