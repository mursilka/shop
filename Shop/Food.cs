﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop1
{
    class Food : Product
    {
        public Food(string name, double price, string country) : base(name, price, country)
        {
            this._id = 1;
        }

        public override Product Add(Product product)
        {

            Food food = new Food(_name, _price, _country);
            return food;
        }

        public override bool Equals(object obj)
        {
            Product product = (Product)obj;

            if (Name == product.Name && Price == product.Price)
            {
                return true;
            }
            else return false;

        }
        public override string ToString()
        {
            return $"Название {_name}\nЦена     {_price}";
        }


    }
}
