﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop1
{
    class Not_food : Product
    {
        public Not_food(string name, double price, string country) : base(name, price, country)
        {
            this._id = 2;
        }

        public override Product Add(Product product)
        {
            Not_food not_Food = new Not_food(_name,_price,_country);
            return not_Food;
        }

        public override bool Equals(object obj)
        {
            Product product = (Product)obj;

            if (Name == product.Name && Price == product.Price)
            {
                return true;
            }
            else return false;
        }

        public override string ToString()
        {
            return $"Название {_name}\nЦена     {_price}";
        }
    }
}
