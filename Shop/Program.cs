﻿
using System;

namespace Shop1
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Shop shop = new Shop();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Выберите действие в магазине:");
                Console.WriteLine("1 Добавить / Удалить товар");
                Console.WriteLine("2 Показать полный список товаров");
                Console.WriteLine("3 Отсортировать товары по цене");
                Console.WriteLine("4 Список съедобных/несъедобных товаров");
                Console.WriteLine("5 Отсортировать товары по производителю");
                Console.WriteLine("6 Выход");

                int _choice = Convert.ToInt32(Console.ReadLine());

                if (_choice == 1)
                {
                    Console.Clear();
                    Console.WriteLine("Выберите действие :");
                    Console.WriteLine("1 Добавить товар в магазин");
                    Console.WriteLine("2 Удалить товар из магазина");

                    int _choice_Add_Del = Convert.ToInt32(Console.ReadLine());

                    if (_choice_Add_Del == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Укажите тип товара:");
                        Console.WriteLine("1 Продовольственный товар");
                        Console.WriteLine("2 Не продовольственный товар");

                        int choice_food_or_not = Convert.ToInt32(Console.ReadLine());

                        if (choice_food_or_not == 1)
                        {
                            shop.AddFood();
                        }
                        if (choice_food_or_not == 2)
                        {
                            shop.AddNotFood();
                        }
                        Console.WriteLine("Товар успешно добавлен");


                    }

                    if (_choice_Add_Del == 2)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите названмие товара");
                        shop.Remove();
                        Console.WriteLine("Товар успешно удален");


                    }

                    Console.WriteLine("Желаете продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "Да" || answer == "да")
                    {
                        continue;
                    }
                    break;

                }

                if (_choice == 2)
                {
                    Console.Clear();
                    shop.Show();

                    Console.WriteLine("Желаете продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "Да" || answer == "да")
                    {
                        continue;
                    }
                    break;
                }
                if (_choice == 3)
                {
                    Console.Clear();
                    shop.Sort_Price();

                    Console.WriteLine("Желаете продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "Да" || answer == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (_choice == 4)
                {
                    Console.Clear();
                    Console.WriteLine("Выберите:");
                    Console.WriteLine("1 Отобразить съедобные товары");
                    Console.WriteLine("2 Отобразить не съедобные товары");
                    shop.Show_Clas();

                    Console.WriteLine("Желаете продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "Да" || answer == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (_choice == 5)
                {
                    Console.Clear();
                    Console.WriteLine("Товар отсортирован по стране производителю");
                    shop.Sort_Country();

                    Console.WriteLine("Желаете продолжить?");
                    string answer = Console.ReadLine();
                    if (answer == "Да" || answer == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (_choice == 6)
                {

                    break;
                }
            };
        }
    }
}
