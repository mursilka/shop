﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop1
{
    abstract class Product
    {
        protected string _name;
        protected double _price;
        protected string _country;
        protected int _id;

        public string Name => _name;
        public double Price => _price;
        public int Id => _id;
        public string Country => _country;
        public Product(string name, double price, string country)
        {
            this._name = name;
            this._price = price;
            this._country = country;
        }

        public abstract Product Add(Product product);

    }


}
